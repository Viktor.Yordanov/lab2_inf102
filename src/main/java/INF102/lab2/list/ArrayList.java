package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		//implement a method that gets the element at index
		if (index < 0 || index >= size()){
			throw new IndexOutOfBoundsException();
		}
		@SuppressWarnings("unchecked")
		T t = (T) this.elements[index];
		return t;
	}
	
	@Override
	public void add(int index, T element) {
		//implement a method that adds element at index
		if (index < 0 || index > size()){
			throw new IndexOutOfBoundsException();
		}
		if (size() == elements.length){
			grow();
		}
		for (int i = size(); i > index; i--){
			elements[i] = elements[i-1];
		}
		elements[index] = element;
		n++;
	}
	
	private void grow() {
		elements = Arrays.copyOf(elements, elements.length*2);
	}

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}